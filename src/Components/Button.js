import React from 'react';
import { Button } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const Example = (props) => {
    return (
      <div>
        <Button color="primary" size="lg">primary</Button>{' '}
        <Button color="link" size="lg">link</Button>
      </div>
    );
  }
  
  export default Example;