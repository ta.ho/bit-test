import React from "react";
import logo from "./logo.svg";
import TestButton from "./Components/Button";
import Navbar from "./Components/Navbar/Navbar";
import NavbarMenu from "./Components/Navbar/NavbarMenu";
import "./App.scss";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Navbar></Navbar>
        <NavbarMenu></NavbarMenu>

        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <TestButton></TestButton>
      </header>
    </div>
  );
}

export default App;
