"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactstrap = require("reactstrap");

require("bootstrap/dist/css/bootstrap.min.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Cards_reactstrap = function Cards_reactstrap(props) {
  return /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement(_reactstrap.Card, null, /*#__PURE__*/_react.default.createElement(_reactstrap.CardImg, {
    top: true,
    width: "100%",
    src: "/assets/318x180.svg",
    alt: "Card image cap"
  }), /*#__PURE__*/_react.default.createElement(_reactstrap.CardBody, null, /*#__PURE__*/_react.default.createElement(_reactstrap.CardTitle, null, "Card title"), /*#__PURE__*/_react.default.createElement(_reactstrap.CardSubtitle, null, "Card subtitle"), /*#__PURE__*/_react.default.createElement(_reactstrap.CardText, null, "Some quick example text to build on the card title and make up the bulk of the card's content."), /*#__PURE__*/_react.default.createElement(_reactstrap.Button, null, "Button"))));
};

var _default = Cards_reactstrap;
exports.default = _default;

//# sourceMappingURL=Cards.js.map